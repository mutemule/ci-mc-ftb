# Container Image for a Minecraft Feed the Beast server

This creates a container image to run a Minecraft server based on the
Feed the Beast Revelation modpack.

My usual approach of a heavily locked down container doesn't work well,
because Minecraft expects to be able to write to its own directory.
Maybe there's a way to fix this, but it's no overly obvious.

This means this container image will not start if you pass `--read-only`
to the container runtime arguments.

# Requirements

A single volume that can be mounted into both this container, as well
as the dnscrypt-proxy container.

# Using

```sh
docker run -d \
    --name mc-ftb \
    --init \
    --volume mc-ftb-world:/minecraft/world \
    --tmpfs /tmp \
    --network games \
    --publish 25565:25565/tcp \
    --cap-drop all \
    --security-opt no-new-privileges \
    --pids-limit 50 \
    registry.gitlab.com/mutemule/ci-mc-ftb/main:latest > "/var/run/mc-ftb.cid"
```
