# Feed the Beast requires Java 8 for now
FROM docker.io/library/openjdk:8-jre AS builder

RUN apt -y update && \
    apt -y install curl jq

ENV FTB_MODPACK_ID="35"

# It seems like modpacks.ch gives us a generic installer, that reads ID and version from the filename
RUN ftb_release_id="$(curl -sL https://api.modpacks.ch/public/modpack/${FTB_MODPACK_ID}/ | jq -r '.versions[-1].id')" && \
    curl -sL https://api.modpacks.ch/public/modpack/${FTB_MODPACK_ID}/${ftb_release_id}/server/linux -o /tmp/installer_${FTB_MODPACK_ID}_${ftb_release_id} && \
    chmod 0755 /tmp/installer_${FTB_MODPACK_ID}_${ftb_release_id} && \
    /tmp/installer_${FTB_MODPACK_ID}_${ftb_release_id} --auto --path /minecraft --integrityupdate


FROM docker.io/library/openjdk:8-jre

LABEL maintainer="Damian Gerow <dwg@flargle.io>"
LABEL name="minecraft-feed-the-beast"

RUN apt -y update && \
    apt -y upgrade && \
    apt -y clean && \
    rm -rf /var/lib/lists/* /tmp/* /var/cache/apt/*

COPY --from=builder /minecraft/ /minecraft/

WORKDIR /minecraft

RUN useradd -c "Minecraft" -M -d /minecraft -s /sbin/nologin -U minecraft && \
    chown -R minecraft:minecraft /minecraft && \
    install -m 0755 -o root -g root -d /minecraft/world && \
    echo -e "$(date)\neula=true" > eula.txt && \
    ln -sf world/server.properties && \
    ln -sf world/whitelist.json && \
    ln -sf world/ops.json && \
    ln -sf world/banned-ips.json && \
    ln -sf world/banned-players.json

USER minecraft

EXPOSE 25565

VOLUME ["/minecraft/world"]

ENTRYPOINT ["/bin/bash", "/minecraft/start.sh"]
